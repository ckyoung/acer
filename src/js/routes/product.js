import Siema from 'siema';

export default {
    init() {
        new Siema({
            selector: '.product-carousel'
        })
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    }
  };