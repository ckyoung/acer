// Based on https://github.com/roots/sage/blob/master/resources/assets/scripts/main.js
import common from './routes/common';
import product from './routes/product';
import Router from './utils/Router';

import AOS from 'aos';

AOS.init({
  once: true
})

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Product page,
  product
});

document.addEventListener("DOMContentLoaded", () => { routes.loadEvents() });