const pug = require('gulp-pug');
const postcss = require('gulp-postcss');
const rename = require("gulp-rename");
const run = require('gulp-run-command').default;
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');

const appPath = {
  local: "node_modules/.app/",
  production: "dist/"
} 

gulp.task('serve', ['pages', 'assets', 'styles', 'scripts'], function() {
  browserSync.init({
      server: {
          baseDir: appPath[process.env.NODE_ENV]
      },
      notify: false
  });

  gulp.watch("src/assets/**/*", ['assets']).on('change', browserSync.reload);
  gulp.watch("src/pages/**/*", ['pages']).on('change', browserSync.reload);
  gulp.watch(["src/css/**/*", 'tailwind.js'], ['styles']).on('change', browserSync.reload);
  gulp.watch("src/js/**/*", ['scripts-reload']);
});

gulp.task('pages', function () {
  return gulp.src('src/pages/*.pug')
    .pipe(pug())
    .pipe(gulp.dest(appPath[process.env.NODE_ENV]));
});

gulp.task('assets', function () {
  gulp.src('src/assets/**/*')
    .pipe(gulp.dest(appPath[process.env.NODE_ENV]));
});

gulp.task('styles', function () {
  return gulp.src('./src/css/main.pcss')
    .pipe(postcss())
    .pipe(cleanCSS())
    .pipe(rename('main.css'))
    .pipe(gulp.dest(appPath[process.env.NODE_ENV] + 'css/'));
});

gulp.task('scripts',  run('parcel build src/js/main.js --no-cache -d ' + appPath[process.env.NODE_ENV] + 'js', {
  quiet: true
}));

gulp.task("scripts-reload", ["scripts"], function() {
  browserSync.reload();
});

gulp.task('build', ['pages', 'assets', 'styles', 'scripts']);
